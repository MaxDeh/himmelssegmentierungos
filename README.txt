A Unity-Project (V. 2020.3.4f1) with mobile AR functionality that implements a color based sky segmentation approach to partially hide objects behind the horizon line. Tested on a Samsung A51 (Android). 

Users need to segment a picture of the landscape. Start the process with the button on the right.

Once the segmentation is finished, a simple cube can be placed via the plus-button. The cube is only shown above the horizon line, i.e. everything that was shown in blue during the segmentation process.

The model can be exchanged with any other model. Simply exchange the prefab and make changes in the project structure accordingly.