using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class ModelManipulator : MonoBehaviour
{
    private GameObject model;
    private Vector3 initialPosition;
    private Vector3 initialRotation;

    [SerializeField] private Transform camera;

    [SerializeField] Text distanceText;
    [SerializeField] Text elevationText;
    [SerializeField] Text horizontalText;
    [SerializeField] Text verticalText;
    [SerializeField] Text rotationText;

    [SerializeField] private Slider elevationSlider;
    [SerializeField] private Slider horizontalSlider;
    [SerializeField] private Slider verticalSlider;
    [SerializeField] private Slider rotationSlider;


    void Start()
    {
        elevationSlider.onValueChanged.AddListener(onElevationSliderChanged);
        horizontalSlider.onValueChanged.AddListener(onHorizontalSliderChanged);
        verticalSlider.onValueChanged.AddListener(onVerticalSliderChanged);
        rotationSlider.onValueChanged.AddListener(onRotationSliderChanged);
        elevationSlider.OnPointerUp(new PointerEventData(EventSystem.current));
    }

    public void SetModel(GameObject modelGameObject)
    {
        model = modelGameObject.gameObject;
        initialPosition = modelGameObject.transform.position;
        initialRotation = modelGameObject.transform.rotation.eulerAngles;
        elevationSlider.value = (elevationSlider.maxValue + elevationSlider.minValue) / 2;
        horizontalSlider.value = (horizontalSlider.maxValue + horizontalSlider.minValue) / 2;
        verticalSlider.value = (verticalSlider.maxValue + verticalSlider.minValue) / 2;
        rotationSlider.value = (rotationSlider.maxValue + rotationSlider.minValue) / 2;
    }

    private void onElevationSliderChanged(float value)
    {
        Vector3 newPos = initialPosition;
        newPos.y += value;
        model.transform.position = newPos;
        UpdateDistanceValue();
    }

    private void onHorizontalSliderChanged(float value)
    {
        Vector3 newPos = initialPosition;
        newPos.x += value;
        model.transform.position = newPos;
        UpdateDistanceValue();
    }

    private void onVerticalSliderChanged(float value)
    {
        Vector3 newPos = initialPosition;
        newPos.z += value;
        model.transform.position = newPos;
        UpdateDistanceValue();
    }

    private void onRotationSliderChanged(float value)
    {
        Vector3 newRot = initialRotation;
        newRot.y += value;
        model.transform.rotation = Quaternion.Euler(newRot.x, newRot.y, newRot.z);
        UpdateDistanceValue();
    }

    public void OnDragStop()
    {
        initialPosition = model.gameObject.transform.position;
        initialRotation = model.gameObject.transform.rotation.eulerAngles;
    }

    private void UpdateDistanceValue()
    {
        float distance = Vector3.Distance(camera.position, model.transform.position);
        distanceText.text = "Distance: " + distance + "m";
    }
}
