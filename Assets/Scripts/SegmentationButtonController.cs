using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

[RequireComponent(typeof(Button))]
public class SegmentationButtonController : MonoBehaviour, IPointerClickHandler
{
    [SerializeField] SegmentationProcessManager segManager;
    public void OnPointerClick(PointerEventData eventData)
    {
        if (segManager.GetSegState() == SegmentationProcessManager.SegState.NotInitialized) segManager.ProgressSegState();
    }
}
