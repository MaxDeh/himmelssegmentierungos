using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class RacastClickDetector : MonoBehaviour, IPointerClickHandler
{
    [SerializeField] private CameraRaycaster raycaster;

    [SerializeField] private GameObject button;

    public void OnPointerClick(PointerEventData eventData)
    {
        raycaster.Raycast(eventData);
    }
}
