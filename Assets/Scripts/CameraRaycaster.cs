using UnityEngine;
using UnityEngine.EventSystems;

[RequireComponent(typeof(Camera))]
public class CameraRaycaster : MonoBehaviour
{
    private Camera cam;
    [SerializeField] private ModelSettingsPanelController panelController;

    public void Raycast(PointerEventData eventData)
    {
        Ray ray = cam.ScreenPointToRay(eventData.position);
        RaycastHit hit;
        if (Physics.Raycast(ray, out hit, 10000))
        {
            GameObject hitObject = hit.collider.gameObject;
            panelController.OpenSettingsPanel(hitObject);
        }
    }

    // Start is called before the first frame update
    void Start()
    {
        cam = GetComponent<Camera>();
    }
}
