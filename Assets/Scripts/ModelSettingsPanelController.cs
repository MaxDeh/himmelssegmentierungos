using UnityEngine;

public class ModelSettingsPanelController : MonoBehaviour
{
    [SerializeField] private GameObject modelSettingsUi;
    [SerializeField] private GameObject segmentationUi;
    [SerializeField] private GameObject arUi;
    [SerializeField] private SegmentationProcessManager segManager;
    [SerializeField] private ModelManipulator manipulator;

    private bool isOpen = false;

    public void OpenSettingsPanel(GameObject gameObject)
    {
        if (segManager.GetSegState() == SegmentationProcessManager.SegState.NotInitialized &&
            isOpen == false)
        {
            segmentationUi.SetActive(false);
            arUi.SetActive(false);
            modelSettingsUi.SetActive(true);
            manipulator.SetModel(gameObject);
            isOpen = true;
        }
    }

    public void CloseSettingsPanel()
    {
        if (isOpen)
        {
            segmentationUi.SetActive(false);
            arUi.SetActive(true);
            modelSettingsUi.SetActive(false);
            isOpen = false;
        }
    }
}
