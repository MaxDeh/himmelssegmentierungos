using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

[RequireComponent(typeof(Button))]
public class ConfirmSettingsButtonController : MonoBehaviour, IPointerClickHandler
{
    [SerializeField] private ModelSettingsPanelController modelSettingsPanelController;

    public void OnPointerClick(PointerEventData eventData)
    {
        modelSettingsPanelController.CloseSettingsPanel();
    }
}
