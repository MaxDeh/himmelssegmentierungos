using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using static SegmentationProcessManager;

[RequireComponent(typeof(Button))]
public class CenterButtonController : MonoBehaviour, IPointerClickHandler
{
    [SerializeField] SegmentationProcessManager segManager;
    [SerializeField] GameObject weaPrefab;
    [SerializeField] Camera cam;
    [SerializeField] GameObject weaObjectHolder;

    public void OnPointerClick(PointerEventData eventData)
    {
        SegState segState = segManager.GetSegState();
        switch (segState)
        {
            case SegState.NotInitialized:
                createWeaModel();
                break;
            case SegState.PictureMode:
                segManager.ProgressSegState();
                break;
            case SegState.ConfirmPicture:
                segManager.ProgressSegState();
                break;
            case SegState.SkyAreaSelection:
                break;
            case SegState.SkyConfirmAreaSelection:
                segManager.ProgressSegState();
                break;
            case SegState.SkyThresholdAdjustment:
                segManager.ProgressSegState();
                break;
            case SegState.CloudAreaSelection:
                break;
            case SegState.CloudConfirmAreaSelection:
                segManager.ProgressSegState();
                break;
            case SegState.CloudThresholdAdjustment:
                segManager.ProgressSegState();
                break;
        }
    }

    void createWeaModel()
    {
        // Get transform of AR camera
        Transform transform = cam.GetComponent<Transform>();

        // Create a vector 500m in front of the field of vision
        Vector3 direction = transform.rotation * Vector3.forward;
        direction.Normalize();
        direction *= 500;

        // Create the 3d object and set the position of it to the created vector
        GameObject newObject = Instantiate(weaPrefab, weaObjectHolder.transform, true);
        newObject.transform.position = direction;
    }
}
