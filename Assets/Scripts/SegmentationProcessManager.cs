using System;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class SegmentationProcessManager : MonoBehaviour
{
    public enum SegState
    {
        NotInitialized,
        PictureMode,
        ConfirmPicture,
        SkyAreaSelection,
        SkyConfirmAreaSelection,
        SkyThresholdAdjustment,
        CloudAreaSelection,
        CloudConfirmAreaSelection,
        CloudThresholdAdjustment
    }

    private enum CalibMode
    {
        Sky,
        Clouds
    }

    [SerializeField] GameObject fpsCounter;
    [SerializeField] GameObject segmentationUi;
    [SerializeField] GameObject arObjectHolder;
    [SerializeField] GameObject selectionCircle;
    [SerializeField] GameObject centerButton;
    [SerializeField] GameObject cancelButton;
    [SerializeField] GameObject startSegmentationButton;
    [SerializeField] GameObject cancelSegmentationButton;
    [SerializeField] GameObject infoTextGameObject;
    [SerializeField] GameObject modelPrefab;

    [SerializeField] Camera backgroundCamera;
    [SerializeField] Camera arCamera;

    [SerializeField] Image snapshotImage;
    [SerializeField] Image overlayImage;
    [SerializeField] Image centerButtonImage;

    [SerializeField] RenderTexture renderTexture;

    [SerializeField] Slider slider;

    [SerializeField] Material weaMat;

    [SerializeField] Text infoTextComponent;

    [SerializeField] Sprite btnSpriteAdd;
    [SerializeField] Sprite btnSpriteSnapshot;
    [SerializeField] Sprite btnSpriteConfirm;
    [SerializeField] Sprite btnSpriteConfirmInactive;

    SegState currState = SegState.NotInitialized;

    Texture2D snapshotTex;
    Texture2D overlayTex;

    Color overlaySkyColor = new Color(0, 0, 1, 0.5f);
    Color overlayGroundColor = new Color(1, 0, 0, 0.5f);

    float avgBlueSky;
    float avgBlueCloud;
    float thresholdSky;
    float thresholdCloud;
    float touchDuration;

    bool touchRegistered = false;

    Vector2 circlePosition;

    private void Awake()
    {
        slider.onValueChanged.AddListener(delegate { OnSliderValueChanged(); });
        ResetMaterial();
    }

    private void Update()
    {
        if (touchRegistered && (currState == SegState.SkyAreaSelection || currState == SegState.CloudAreaSelection))
        {
            touchDuration += Time.deltaTime;
            AdjustSelectionCircle();
        }

        if (Input.GetKeyDown(KeyCode.Escape)) RegressSegState();
    }

    public void ProgressSegState()
    {
        switch (currState)
        {
            case SegState.NotInitialized:
                EnterPictureMode();
                break;
            case SegState.PictureMode:
                CreateSnapshot();
                EnterConfirmPictureMode();
                break;
            case SegState.ConfirmPicture:
                EnterSkyAreaSelectionMode();
                break;
            case SegState.SkyAreaSelection:
                EnterSkyConfirmAreaSelectionMode();
                break;
            case SegState.SkyConfirmAreaSelection:
                EnterSkyThresholdAdjustmentMode();
                break;
            case SegState.SkyThresholdAdjustment:
                EnterCloudAreaSelectionMode();
                break;
            case SegState.CloudAreaSelection:
                EnterCloudConfirmAreaSelectionMode();
                break;
            case SegState.CloudConfirmAreaSelection:
                EnterCloudThresholdAdjustmentMode();
                break;
            case SegState.CloudThresholdAdjustment:
                OnConfirmSegmentation();
                break;
        }
    }

    public void RegressSegState()
    {
        switch (currState)
        {
            case SegState.NotInitialized:
                break;
            case SegState.PictureMode:
                EnterNotInitializedMode();
                break;
            case SegState.ConfirmPicture:
                EnterPictureMode();
                break;
            case SegState.SkyAreaSelection:
                EnterPictureMode();
                break;
            case SegState.SkyConfirmAreaSelection:
                EnterSkyAreaSelectionMode();
                break;
            case SegState.SkyThresholdAdjustment:
                EnterSkyConfirmAreaSelectionMode();
                break;
            case SegState.CloudAreaSelection:
                EnterSkyAreaSelectionMode();
                break;
            case SegState.CloudConfirmAreaSelection:
                EnterCloudAreaSelectionMode();
                break;
            case SegState.CloudThresholdAdjustment:
                EnterCloudConfirmAreaSelectionMode();
                break;
        }
    }

    public SegState GetSegState()
    {
        return currState;
    }

    void EnterNotInitializedMode()
    {
        currState = SegState.NotInitialized;

        centerButtonImage.sprite = btnSpriteAdd;
        infoTextComponent.text = "";

        fpsCounter.SetActive(true);
        cancelSegmentationButton.SetActive(false);
        segmentationUi.SetActive(false);
        snapshotImage.gameObject.SetActive(false);
        overlayImage.gameObject.SetActive(false);
        slider.gameObject.SetActive(false);
        selectionCircle.SetActive(false);
        cancelButton.SetActive(false);
        startSegmentationButton.SetActive(true);
        infoTextGameObject.SetActive(false);
    }

    void EnterPictureMode()
    {
        currState = SegState.PictureMode;

        centerButtonImage.sprite = btnSpriteSnapshot;
        infoTextComponent.text = "Take a picture of the landscape!";

        fpsCounter.SetActive(false);
        segmentationUi.SetActive(true);
        snapshotImage.gameObject.SetActive(false);
        overlayImage.gameObject.SetActive(false);
        slider.gameObject.SetActive(false);
        selectionCircle.SetActive(false);
        cancelButton.SetActive(true);
        startSegmentationButton.SetActive(false);
        cancelSegmentationButton.SetActive(true);
        infoTextGameObject.SetActive(true);
    }

    void EnterConfirmPictureMode()
    {
        currState = SegState.ConfirmPicture;

        CreateSnapshot();

        centerButtonImage.sprite = btnSpriteConfirm;
        infoTextComponent.text = "Is the picture okay?";

        fpsCounter.SetActive(false);
        segmentationUi.SetActive(true);
        snapshotImage.gameObject.SetActive(true);
        overlayImage.gameObject.SetActive(false);
        slider.gameObject.SetActive(false);
        selectionCircle.SetActive(false);
        cancelButton.SetActive(true);
        startSegmentationButton.SetActive(false);
        infoTextGameObject.SetActive(true);
    }

    void EnterSkyAreaSelectionMode()
    {
        currState = SegState.SkyAreaSelection;

        centerButtonImage.sprite = btnSpriteConfirmInactive;
        infoTextComponent.text = "Tap and hold the sky!";

        fpsCounter.SetActive(false);
        segmentationUi.SetActive(true);
        snapshotImage.gameObject.SetActive(true);
        overlayImage.gameObject.SetActive(false);
        slider.gameObject.SetActive(false);
        selectionCircle.SetActive(false);
        cancelButton.SetActive(false);
        startSegmentationButton.SetActive(false);
        infoTextGameObject.SetActive(true);
    }

    void EnterSkyConfirmAreaSelectionMode()
    {
        currState = SegState.SkyConfirmAreaSelection;

        centerButtonImage.sprite = btnSpriteConfirm;
        infoTextComponent.text = "Is the selection okay?";

        fpsCounter.SetActive(false);
        segmentationUi.SetActive(true);
        snapshotImage.gameObject.SetActive(true);
        overlayImage.gameObject.SetActive(false);
        slider.gameObject.SetActive(false);
        selectionCircle.SetActive(true);
        cancelButton.SetActive(true);
        startSegmentationButton.SetActive(false);
        infoTextGameObject.SetActive(true);
    }

    void EnterSkyThresholdAdjustmentMode()
    {
        currState = SegState.SkyThresholdAdjustment;

        avgBlueSky = CalculateAverageSelectedColor();
        GenerateOverlaySprite(CalibMode.Sky);

        centerButtonImage.sprite = btnSpriteConfirm;
        infoTextComponent.text = "Adjust the slider until only the sky is tinted blue!";

        fpsCounter.SetActive(false);
        segmentationUi.SetActive(true);
        snapshotImage.gameObject.SetActive(true);
        overlayImage.gameObject.SetActive(true);
        slider.gameObject.SetActive(true);
        selectionCircle.SetActive(true);
        cancelButton.SetActive(true);
        startSegmentationButton.SetActive(false);
        infoTextGameObject.SetActive(true);
    }

    void EnterCloudAreaSelectionMode()
    {
        currState = SegState.CloudAreaSelection;

        centerButtonImage.sprite = btnSpriteConfirmInactive;
        infoTextComponent.text = "Tap and hold the clouds!";

        fpsCounter.SetActive(false);
        segmentationUi.SetActive(true);
        snapshotImage.gameObject.SetActive(true);
        overlayImage.gameObject.SetActive(false);
        slider.gameObject.SetActive(false);
        selectionCircle.SetActive(false);
        cancelButton.SetActive(false);
        startSegmentationButton.SetActive(false);
        infoTextGameObject.SetActive(true);
    }

    void EnterCloudConfirmAreaSelectionMode()
    {
        currState = SegState.CloudConfirmAreaSelection;

        centerButtonImage.sprite = btnSpriteConfirm;
        infoTextComponent.text = "Is the selection okay?";

        fpsCounter.SetActive(false);
        segmentationUi.SetActive(true);
        snapshotImage.gameObject.SetActive(true);
        overlayImage.gameObject.SetActive(false);
        slider.gameObject.SetActive(false);
        selectionCircle.SetActive(true);
        cancelButton.SetActive(true);
        startSegmentationButton.SetActive(false);
        infoTextGameObject.SetActive(true);
    }

    void EnterCloudThresholdAdjustmentMode()
    {
        currState = SegState.CloudThresholdAdjustment;

        avgBlueCloud = CalculateAverageSelectedColor();
        GenerateOverlaySprite(CalibMode.Clouds);

        centerButtonImage.sprite = btnSpriteConfirm;
        infoTextComponent.text = "Adjust the slider until the sky and the clouds are tinted blue!";

        fpsCounter.SetActive(false);
        segmentationUi.SetActive(true);
        snapshotImage.gameObject.SetActive(true);
        overlayImage.gameObject.SetActive(true);
        slider.gameObject.SetActive(true);
        selectionCircle.SetActive(true);
        cancelButton.SetActive(true);
        startSegmentationButton.SetActive(false);
        infoTextGameObject.SetActive(true);
    }

    public void OnConfirmSegmentation()
    {
        weaMat.SetFloat("_ThresholdSky", thresholdSky);
        weaMat.SetFloat("_BlueValSky", avgBlueSky);
        weaMat.SetFloat("_ThresholdCloud", thresholdCloud);
        weaMat.SetFloat("_BlueValCloud", avgBlueCloud);
        MeshRenderer[] rendererArr = modelPrefab.GetComponentsInChildren<MeshRenderer>();
        for (int i = 0; i < rendererArr.Length; i++)
        {
            rendererArr[i].sharedMaterial = weaMat;
        }
        segmentationUi.SetActive(false);
        centerButtonImage.sprite = btnSpriteAdd;
        EnterNotInitializedMode();
    }

    public void CancelSegProcess()
    {
        weaMat.SetFloat("_ThresholdSky", 1f);
        weaMat.SetFloat("_BlueValSky", 1f);
        weaMat.SetFloat("_ThresholdCloud", 1f);
        weaMat.SetFloat("_BlueValCloud", 1f);
        MeshRenderer[] rendererArr = modelPrefab.GetComponentsInChildren<MeshRenderer>();
        for (int i = 0; i < rendererArr.Length; i++)
        {
            rendererArr[i].sharedMaterial = weaMat;
        }
        segmentationUi.SetActive(false);
        centerButtonImage.sprite = btnSpriteAdd;
        EnterNotInitializedMode();
    }

    void ResetMaterial()
    {
        weaMat.SetFloat("_ThresholdSky", 1f);
        weaMat.SetFloat("_BlueValSky", 1f);
        weaMat.SetFloat("_ThresholdCloud", 1f);
        weaMat.SetFloat("_BlueValCloud", 1f);
        MeshRenderer[] rendererArr = modelPrefab.GetComponentsInChildren<MeshRenderer>();
        for (int i = 0; i < rendererArr.Length; i++)
        {
            rendererArr[i].sharedMaterial = weaMat;
        }
    }

    void OnSliderValueChanged()
    {
        if (currState == SegState.SkyThresholdAdjustment)
        {
            thresholdSky = slider.value;
            GenerateOverlaySprite(CalibMode.Sky);
        }
        if (currState == SegState.CloudThresholdAdjustment)
        {
            thresholdCloud = slider.value;
            GenerateOverlaySprite(CalibMode.Clouds);
        }
    }

    public void CreateSnapshot()
    {
        snapshotTex = renderTexToTex2D(renderTexture);
        snapshotImage.gameObject.SetActive(true);
        snapshotImage.sprite = GenerateSprite(snapshotTex);
    }

    void AdjustSelectionCircle()
    {
        selectionCircle.transform.position = circlePosition;
        selectionCircle.transform.localScale = Vector3.one * touchDuration * 2;
    }

    float CalculateAverageSelectedColor()
    {
        int xCoord = (int)(circlePosition.x * ((float)snapshotImage.sprite.texture.width / (float)Screen.width));
        int yCoord = (int)(circlePosition.y * ((float)snapshotImage.sprite.texture.height / (float)Screen.height));
        float circleRadius = ((selectionCircle.transform.localScale.x * 256f) / (float)Screen.width) * 0.5f * (float)snapshotImage.sprite.texture.width;
        int textureWidth = snapshotImage.sprite.texture.width;
        int textureHeight = snapshotImage.sprite.texture.height;
        int addedPixels = 0;
        Color[] selectedColors = snapshotImage.sprite.texture.GetPixels();
        float avgBlue = 0;

        for (int i = 0; i < textureHeight; i++)
        {
            for (int j = 0; j < textureWidth; j++)
            {
                float distance = Mathf.Sqrt(Mathf.Pow(j - xCoord, 2) + Mathf.Pow(i - yCoord, 2));
                if (distance < circleRadius)
                {
                    avgBlue += selectedColors[j + i * textureWidth].b;
                    addedPixels++;
                }
            }
        }
        avgBlue = avgBlue / (float)addedPixels;
        return avgBlue;
    }

    public void OnFingerDown(PointerEventData eventData)
    {
        selectionCircle.SetActive(true);
        touchRegistered = true;
        circlePosition = eventData.position;
    }

    public void OnFingerUp(PointerEventData eventData)
    {
        touchRegistered = false;
        touchDuration = 0;
        if (currState == SegState.SkyAreaSelection || currState == SegState.CloudAreaSelection) ProgressSegState();
    }

    Texture2D renderTexToTex2D(RenderTexture renderTex)
    {
        Texture2D tex = new Texture2D(renderTex.width, renderTex.height, TextureFormat.RGBA32, false);
        RenderTexture.active = renderTex;
        tex.ReadPixels(new Rect(0, 0, renderTex.width, renderTex.height), 0, 0);
        tex.Apply();
        return tex;
    }

    Sprite GenerateSprite(Texture2D texture)
    {
        Sprite sprite = Sprite.Create(texture, new Rect(0, 0, texture.width, texture.height), new Vector2(0.5f, 0.5f), 100f);
        sprite.name = "snapshotSprite";
        return sprite;
    }

    void GenerateOverlaySprite(CalibMode mode)
    {
        try
        {
            DestroyImmediate(overlayImage.sprite.texture, true);
            DestroyImmediate(overlayImage.sprite, true);
        }
        catch (Exception e)
        {
            
        }
        
        overlayTex = new Texture2D(snapshotTex.width, snapshotTex.height, TextureFormat.RGBA32, false);

        overlayTex.SetPixels(ClassifyTexture(mode));
        
        overlayTex.Apply();

        overlayImage.enabled = true;
        
        overlayImage.sprite = GenerateSprite(overlayTex);
    }

    Color[] ClassifyTexture(CalibMode mode)
    {
        Color[] colorArr = snapshotTex.GetPixels();
        int length = colorArr.Length;
        Color[] result = new Color[length];
        int[] samplePositions = new int[4];
        int width = snapshotTex.width;
        for (int i = 0; i < colorArr.Length; i++)
        {
            int leftOffset = (i % width == 0) ? 0 : -1;
            int rightOffset = (i % width == (width - 1)) ? 0 : 1;
            int bottomOffset = (i < width) ? 0 : -1;
            int topOffset = (i >= (length - width)) ? 0 : 1;
            samplePositions[0] = i + (topOffset * width) + leftOffset;
            samplePositions[1] = i + (topOffset * width) + rightOffset;
            samplePositions[2] = i + (bottomOffset * width) + leftOffset;
            samplePositions[3] = i + (bottomOffset * width) + rightOffset;
            if (ClassifyPixel(colorArr[samplePositions[0]], mode) &&
                ClassifyPixel(colorArr[samplePositions[0]], mode) &&
                ClassifyPixel(colorArr[samplePositions[0]], mode) &&
                ClassifyPixel(colorArr[samplePositions[0]], mode)) result[i] = overlaySkyColor;
            else result[i] = overlayGroundColor;
        }
        return result;
    }

    bool ClassifyPixel(Color color, CalibMode mode)
    {
        switch (mode)
        {
            case CalibMode.Sky:
                if (Mathf.Abs(color.b - avgBlueSky) < thresholdSky) return true;
                break;
            case CalibMode.Clouds:
                if ((Mathf.Abs(color.b - avgBlueSky) < thresholdSky) || (Mathf.Abs(color.b - avgBlueCloud) < thresholdCloud)) return true;
                break;
        }
        return false;
    }
}
