using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class FpsCounter : MonoBehaviour
{
    [SerializeField] float frequency = 1.0f;
    [SerializeField] private string displayString = "FPS: ";

    private Text text;
    private string fps;

    void Start()
    {
        text = GetComponent<Text>();
        StartCoroutine(FPS());
    }

    private void OnEnable()
    {
        StartCoroutine(FPS());
    }

    private IEnumerator FPS()
    {
        while (true)
        {
            int lastFrameCount = Time.frameCount;
            float lastTime = Time.realtimeSinceStartup;
            yield return new WaitForSeconds(frequency);
            float timeSpan = Time.realtimeSinceStartup - lastTime;
            int frameCount = Time.frameCount - lastFrameCount;
            fps = string.Format(displayString + "{0}", Mathf.RoundToInt(frameCount / timeSpan));
            text.text = fps;
        }
    }
}
